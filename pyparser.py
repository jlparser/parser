import re
from collections import OrderedDict
from typing import Union
import numpy as np

def _strToObject(value: str, _type):
    """
    Convert a string to an object with type given by _type

    :param value: a string representing an object of type _type
    :param _type: the target _type
    """
    if _type == 'int' or _type == 'long':
        value = int(value)
    elif _type == 'float':
        value = float(value)
    elif _type == 'vector':
        vals = value.split(' ')
        value = np.array([float(x) for x in vals])
    return value


def _formatItem(item):
    """
    Format a Parser entry

    :param item: a dictionary with two keys 'type' and 'value'
    """
    if item['type'] != 'vector':
        return str(item['value'])
    v = item['value'][0]
    if np.prod([x == v for x in item['value']]):
        return str(v)
    ans = ''
    for x in item['value']:
        ans += str(x) + ' '
    return ans.strip()


class Parser:
    """
    A Mapping class to read problem files using the syntax

        key <type> value

    The content of the file is stored in a dictionary indexed by key holding
    the pairs (value, type) as dictionary
    """
    TYPE_MAX_LENGTH = 6 + 4

    def __init__(self, data: Union[str, None] = None):
        self.data = OrderedDict()
        if isinstance(data, str):
            with open(data, encoding='utf8') as fd:
                for line in fd:
                    line = str.strip(re.sub(r'#.*$', '', line))
                    if line == '':
                        continue
                    toks = re.split(r'<|>', line)
                    if len(toks) != 3:
                        raise Exception('Format error in ' + data)
                    _label = str.strip(toks[0])
                    _type = str.strip(toks[1])
                    _value = _strToObject(str.strip(toks[2]), _type)
                    self.data[_label] = {'type': _type, 'value': _value}

    def __str__(self):
        return str(self.data)

    def get(self, label, raiseError=True, defaultValue=None):
        try:
            return self.data[label]['value']
        except KeyError as e:
            if not raiseError:
                return defaultValue
            else:
                print('Key ' + label + ' not found.')
                raise e


    def getVector(self, label, n, raiseError=True):
        try:
            if self.data[label]['type'] != 'vector':
                raise 'Element ' + label + ' is not of type vector'
            v = self.data[label]['value']
            if np.size(v) == 1:
                return np.full(n, v)
            else:
                return v
        except KeyError as e:
            if not raiseError:
                return None
            else:
                print('Key ' + label + ' not found.')
                raise e

    def write(self, fd):
        """
        Write the Parser content to a file object

        :param fd: file object
        """
        keyMaxLength = max(len(k) for k in self.data.keys()) + 4
        for key, item in self.data.items():
            keyStr = key + ' ' * (keyMaxLength - len(key))
            itemStr = '<' + item['type'] + '>' + ' ' * (self.TYPE_MAX_LENGTH - len(item['type']))
            print(keyStr, itemStr, _formatItem(item), file=fd)


    def set(self, key, value, _type=None):
        """
        Set the value of a key. If the key already exists in the Parser, the
        _type parameter is not used

        :param key: the key to be set
        :param value: the value associated to the key. It can given as the real object
        value or as a string representing the object
        :param _type: Optional if key is already present in the Parser
        """
        if _type is None and key not in self.data:
            raise ValueError('Cannot insert a new element with no type')
        if _type is None:
            _type = self.data[key]['type']
        value = _strToObject(value, _type)
        if key in self.data:
            self.data[key]['value'] = value
        else:
            self.data[key] = {'value': value, 'type': _type}
