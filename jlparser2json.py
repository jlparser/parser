#!/usr/bin/env python

from pathlib import Path
import sys
import json
from json import JSONEncoder
import numpy
import pyparser

class NumpyArrayEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, numpy.ndarray):
            return o.tolist()
        return JSONEncoder.default(self, o)

def convert_parser_to_json(fparser):
    fjson = Path(fparser).with_suffix('.json')
    params = pyparser.Parser(fparser)
    print(f'Converting {fparser} to {fjson}')
    keyValueObjects = {}
    for key, valType in params.data.items():
        value = valType['value']
        keyValueObjects[key] = value
    with open(fjson, mode='w', encoding='utf-8') as fd:
        json.dump(keyValueObjects, fd, cls=NumpyArrayEncoder, indent=4)

def print_usage():
    print('python jlparser2json file1 ...')
    print('\tFor every input file, a json with the same name is created.')

if __name__ == "__main__":
    if len(sys.argv) == 1:
        print_usage()
        exit(0)
    FILES = sys.argv[1:]
    for f in FILES:
        convert_parser_to_json(f)
